# Reflection in Java

* An API used for analysis & modification of classes & class members like constructors, fields(variables), methods etc.

* Can be implemented using classes in **_java.lang.reflect_** package and methods of **_java.lang.Class_**

* Can be used to access & modify private fields & methods

* Usually used during debugging

### Java classes used in reflections:
    
Class | Accessible information
------------ | -------------
Field | datatype, access modifier, name and value
Method | access modifier, return type, name, parameter types
Constructor | access modifier, name and parameter types
Modifier | info related to access modifiers

### Java methods used in reflections:

Method | Functionality
------------ | -------------
getName() | returns name of the class
getSuperClass() | return super class reference
getInterfaces() | returns array of interfaces implemented
getModifiers() | returns number of access modifiers used

### Example: 
#### ReflectApp.java
```
    public class ReflectApp extends Student implements Teacher{
	
	public String name;
	public int id;
	
	public void getName() {
		System.out.println("Teacher's name: "+"Anurag");
	}
	
	public static void main(String[]args) {
		Object reflectAppObj = new ReflectApp();
		
		Class reflectAppClass = reflectAppObj.getClass();
		
		System.out.println(reflectAppClass.getName());
		
		System.out.println(reflectAppClass.getSuperclass().getName());
		
		Class[] reflectAppInterfacesList = reflectAppClass.getInterfaces();
		
		for(Class reflectAppInterface: reflectAppInterfacesList) {
			System.out.println(reflectAppInterface.getName());
		}
		
		int numOfModifiers = reflectAppClass.getModifiers();
		System.out.println(Modifier.toString(numOfModifiers));
		
		Field[] reflectAppFields = reflectAppClass.getDeclaredFields();
		
		for(Field reflectAppField: reflectAppFields) {
			System.out.println("variable: "+reflectAppField.getName()+", "+"type: "+reflectAppField.getType());
		}
	}
}
```
#### Student.java
```
    public class Student {
	private static void secretMethod() {
		System.out.println("The secret id is 11267");
	}
}
```
#### Teacher.java
```
    public interface Teacher {
	public void getName();
}
```
### Output: 
![Output](output.png)

### Resources:

* https://www.guru99.com/java-reflection-api.html
