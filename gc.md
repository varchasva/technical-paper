# Garbage Collection in Java

* Used by Java programs for automatic memory management.

* Bytecode is generated after compilation of every Java program.

* This bytecode runs on an abstract virtual machine, known as **_Java Virtual Machine_**.

* When the bytecode is running on the JVM, objects are created on a dedicated area in the memory, known as a **_heap_**.

* Once the object has been used, it may no longer be required in the program.

* Here comes the Garbage Collector, which will delete these objects to free up the memory.

* To make an object **eligible** for Garbage Collection, it must be **de-referenced** from any references, i.e. it must be **unreachable**.

##  Making objects 'unreachable' in Java

### 1. By nullifying the reference variable 

```
    public class Main {
        Student obj = new Student(); //currently the Student object is not eligible for Garbage Collection

        obj = null; //Now, the object is unreachable & eligible for Garbage Collection
    }
```

### 2. By creating the object inside a method

```
    public class Main {
        public void fun() {
            Main obj1 = new Main(); //This object will be lost once function 'fun' gets removed from call stack
        }

        public static void main(String[]args) {
            Main obj2 = new Main();
            obj2.fun(); //This object will be lost once function 'main' gets removed from call stack
        }
    }
```
Functions are called using a call-stack. As & when a function gets called, it is removed from the call stack, thus **de-referencing* any objects created inside it. This makes them eligible for Garbage Collection.

### 3. By reassigning the reference variable to another object

```
    public class Main {
        public static void main(String[]args) {
            Main obj1 = new Main();
            Main obj2 = new Main();

            obj1 = obj2; //Now object 'obj1' is no more being referenced, thus eligible for Garbage Collection
        }
    }
```

### 4. By making an anonymous object

```
    public class Main {
        public static void main(String[]args) {
            new Main(); //This object is not being referenced by any variable, thus available for Garbage Collection
        }
    }
```

### 5. By creating an **_Island of Isolation_** 

An _Island of Isolation_ is a group of objects referencing each other, & not being referenced by any other _active_ object.

```
    class Main {
        Main m;
        public static void main(String[]args) {
            Main obj1 = new Main();
            Main obj2 = new Main();

            obj1.m = obj2;
            obj2.m = obj1;

            obj1 = null; 
            obj2 = null; //Now both the objects are unreachable, & thus available for Garbage Collection
        }
    }
```

## How to request the JVM to run the Garbage Collector?

Invoke **_System.gc()_** method

```
    public class Main {
        Employee emp = new Employee();

        emp = null;

        System.gc(); //Requesting the JVM to run GC
    }
```
Note: _**Although the above call to System class does request the Garbage Collector to be invoked, but there is no guarantee that it will definitely be invoked.**_